#!/usr/bin/env python

import argparse
import os
import os.path
import sys
import json
import re


COLOR_ATTRIBUTES = ["client.focused", "client.focused_inactive", "client.unfocused", "focused_workspace", "active_workspace", "inactive_workspace", "urgent_workspace", "statusline", "background"]


def string_found(string1, string2):
   if re.search(r"\b" + re.escape(string1) + r"\b", string2):
      return True
   return False


def parseArguments():
    parser = argparse.ArgumentParser(description="Patch themes into i3 config")
    parser.add_argument("option", help="'patch' a theme into i3 config, 'rip' theme from i3 config")
    parser.add_argument("config", help="path to i3 config")
    parser.add_argument("-t", "--theme", help="path to theme file for patching")
    parser.add_argument("-o", "--output", help="output path of ripped theme")
    args = parser.parse_args()
    return args


def ripScheme(config):
    colorDict = dict()
    for index, line in enumerate(configFile):
        for color in COLOR_ATTRIBUTES:
            if color in colorDict.keys():
                continue
            if string_found(color, line):
                if line.strip()[:1] != "#": # Ignore code comments
                    colorDict[color] = line.replace('\n', '').replace(color, "").strip()
                    temp = colorDict[color].split()
                    colorDict[color] = ' '.join(temp)

    jsonList = json.dumps(colorDict, indent=4)

    return jsonList


def patchScheme(config, theme):
    for index, line in enumerate(config):
        for color in theme.keys():
            if string_found(color, line):
                whitespace = len(line) - len(line.lstrip())
                #config[index] = ('\t' * whitespace) + color + "\t" + theme[color] + "\n"
                endIndex = 0
                for i, char in enumerate(line):
                    if char != '\t' and char != ' ':
                        endIndex = i
                        break

                if endIndex > 0:
                    config[index] = line[:endIndex] + color + '\t' + theme[color] + "\n"
                else:
                    config[index] = color + "\t" + theme[color] + "\n"
                
    return ''.join(config)


# BEGIN EXECUTION
args = parseArguments()

path = ""
if args.config and os.path.exists(os.path.dirname(args.config)):
    path = args.config
else:
    print("Config path doesn't exist")
    sys.exit(1)

configFile = ""
with open(path, "r") as fileHandle:
    configFile = fileHandle.readlines()

if len(configFile) == 0:
    print("Reading of config file failed, or file length is zero")
    sys.exit(1)

if args.option == "rip":
    if args.output and len(args.output):
        scheme = ripScheme(configFile)

        with open(args.output, 'w') as jsonFile:
            jsonFile.write(scheme)
    else:
        print("No output dir specified")
elif args.option == "patch":
    if args.output and len(args.output):
        if os.path.isfile(args.theme):
            jsonFile = None
            with open(args.theme, 'r') as themeFile:
                jsonFile = json.loads(themeFile.read())

            if jsonFile != None:
                patchedConf = patchScheme(configFile, jsonFile)
                with open(args.output, 'w') as configHandle:
                    configHandle.write(patchedConf)
            else:
                print("Failed to parse theme json")
        else:
            print("theme file doesn't exist")
